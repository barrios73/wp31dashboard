import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wps-algorithm-page',
  templateUrl: './wps-algorithm-page.component.html',
  styleUrls: ['./wps-algorithm-page.component.scss']
})
export class WpsAlgorithmPageComponent implements OnInit {

  public pageTitle: string 
  public openRelief = false;
  public openFCBF = false;
  public openJMIM = false;
  public openWindowOvl = false;

  constructor() { 
    this.pageTitle = "Algorithms"
  }

  ngOnInit(): void {
  }


  public close(algorithm): void {
    if (algorithm == 'Relief'){
      this.openRelief = false;
      this.openWindowOvl = false;
    }
    else if (algorithm == 'FCBF'){
      this.openFCBF = false;
      this.openWindowOvl = false;
    }
    else if (algorithm == 'JMIM'){
      this.openJMIM = false;
      this.openWindowOvl = false;
    }
  }

  public open(algorithm): void {
    if (algorithm == 'Relief'){
      this.openRelief = true;
      this.openWindowOvl = true;
    }
    else if (algorithm == 'FCBF'){
      this.openFCBF = true;
      this.openWindowOvl = true;
    }
    else if (algorithm == 'JMIM'){
      this.openJMIM = true;
      this.openWindowOvl = true;
    }
  }

}
