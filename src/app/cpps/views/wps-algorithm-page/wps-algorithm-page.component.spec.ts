import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsAlgorithmPageComponent } from './wps-algorithm-page.component';

describe('WpsAlgorithmPageComponent', () => {
  let component: WpsAlgorithmPageComponent;
  let fixture: ComponentFixture<WpsAlgorithmPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsAlgorithmPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsAlgorithmPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
