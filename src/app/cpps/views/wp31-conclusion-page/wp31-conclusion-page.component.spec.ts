import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wp31ConclusionPageComponent } from './wp31-conclusion-page.component';

describe('Wp31ConclusionPageComponent', () => {
  let component: Wp31ConclusionPageComponent;
  let fixture: ComponentFixture<Wp31ConclusionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Wp31ConclusionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wp31ConclusionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
