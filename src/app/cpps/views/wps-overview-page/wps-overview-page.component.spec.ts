import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsOverviewPageComponent } from './wps-overview-page.component';

describe('WpsOverviewPageComponent', () => {
  let component: WpsOverviewPageComponent;
  let fixture: ComponentFixture<WpsOverviewPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsOverviewPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
