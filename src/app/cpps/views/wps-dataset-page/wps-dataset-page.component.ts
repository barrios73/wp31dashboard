import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wps-dataset-page',
  templateUrl: './wps-dataset-page.component.html',
  styleUrls: ['./wps-dataset-page.component.scss']
})
export class WpsDatasetPageComponent implements OnInit {
  public scroll= 'none';
  comparison_table = [
    {
      theoretical  : {
        name: 'Colon (Academic Dataset 1)',
        type: 'Medical',
        description: 'Binary classification task for 2000 binary features, diagnostic result for each of 62 samples (abnormal or normal). The features (inputs) represent the colon cancer gene expression data.'
      } ,
      academic: {
        name: 'Academic Dataset 1',
        type: 'Medical',
        description: 'Binary classification task for 2000 binary features, diagnostic result for each of 62 samples (abnormal or normal).'
      },
      industry: {
        name: 'Industrial Dataset',
        type: 'Bioinstrumentation Manufacturing',
        description: 'Product quality classification from 156 process parameters and 74 material inspection parameters to identify major factors for determining the product failure root causes.'
      }
    },
    {
      theoretical  : {
        name: 'Multi Objective',
        type: 'Deb and Gupta, 2006',
        description: 'Analyse effect of sensitivity to uncertainty in bi-objective optimization with noisy decision variables.'
      } ,
      academic: {
        name: 'Academic Dataset 2',
        type: 'Medical',
        description: ' Multi-class classification task, diagnosis of erythemato-squamous disease into 6 types(psoriasis, seboreic dermatitis, lichen planus, \n' +
          '            pityriasis rosea, cronic dermatitis, and pityriasis rubra pilaris).'
      },
      industry: {
         name: '',
         type: '',
         description: ''
      }
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
