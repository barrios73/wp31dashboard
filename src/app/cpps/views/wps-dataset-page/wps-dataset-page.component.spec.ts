import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsDatasetPageComponent } from './wps-dataset-page.component';

describe('WpsDatasetPageComponent', () => {
  let component: WpsDatasetPageComponent;
  let fixture: ComponentFixture<WpsDatasetPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsDatasetPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsDatasetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
