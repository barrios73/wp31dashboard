import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsConclusionPageComponent } from './wps-conclusion-page.component';

describe('WpsConclusionPageComponent', () => {
  let component: WpsConclusionPageComponent;
  let fixture: ComponentFixture<WpsConclusionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsConclusionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsConclusionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
