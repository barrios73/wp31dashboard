import {Component, OnInit, ViewChild} from '@angular/core';
import {DataBindingDirective} from '@progress/kendo-angular-grid';
import {employees} from '@dis/services/mocks/sampleDataForGrid';
import { process } from '@progress/kendo-data-query';
import {chartConfig} from '@dis/settings/chart.config';

@Component({
  selector: 'app-wps-conclusion-page',
  templateUrl: './wps-conclusion-page.component.html',
  styleUrls: ['./wps-conclusion-page.component.scss']
})
export class WpsConclusionPageComponent implements OnInit {

  chartConfig = chartConfig;

  constructor() { }

  ngOnInit(): void {
  }

  public labelContentFrom(e): string {
    return `${ e.value.from }`;
  }

  public labelContentTo(e): string {
    return `${ e.value.to } `;
  }
}
