import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsResultsPageComponent } from './wps-results-page.component';

describe('WpsResultsPageComponent', () => {
  let component: WpsResultsPageComponent;
  let fixture: ComponentFixture<WpsResultsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsResultsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
