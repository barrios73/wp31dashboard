import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import * as io from 'socket.io-client';
import {combineLatest, forkJoin} from "rxjs";
import {combineAll, tap} from "rxjs/operators";

//defines the different algorithms and datasets used
interface algorithm {
  algoName: string,
  selected: boolean
}

//defines the different algorithms and datasets used
interface dataset {
  datasetName: string,
  selected: boolean
}

interface f1AvgScore {
  algorithm: string,
  dataset: string,
  f1Avg: number,
  accuracy: number
}

interface algoResult {
  Algorithm: string,
  Featlist: string[],
  F1score: number
}

@Component({
  selector: 'app-wps-results-page',
  templateUrl: './wps-results-page.component.html',
  styleUrls: ['./wps-results-page.component.scss']
})


export class WpsResultsPageComponent implements OnInit {

  public pageTitle: string;
  public categoriesPrimary = [];  //for algo
  public categoriesSecondary = [];  //for dataset
  public algoCategories = [];
  public datasetCategories = [];
  public algorithmList: Array<algorithm>
  public datasetList: Array<dataset>
  public f1AvgScore: Array<f1AvgScore>
  public algoPerformance: Array<algoResult>
  public selectedFeatures: any[]

  public showSelectedFeatures: boolean = false;

  public chartData = []

  public cbAlgoHybrid: boolean
  public cbAlgoRelief: boolean
  public cbAlgoFCBF: boolean
  public cbAlgoJMIM: boolean
  public cbDatasetColon: boolean
  public cbDatasetDermatology: boolean
  public cbDatasetAFPD: boolean
  public cbDatasetAgilent: boolean
  private socketio: io.Socket
  public featlist: string[]


  constructor(private http: HttpClient) {
    this.pageTitle = "Results";

  }

  ngOnInit(): void {

    //this.socketConnect();
    //this.monitorRelief();
    //this.monitorFCBF();
    //this.monitorJMIM();

    // this.socketio.on('fcbf', (msg: any) => {
    //   //this.messages.push(msg);
    //   console.log(msg.Featlist);
    //   console.log(msg.F1score);

    // });

    // this.socketio.on('hybrid', (msg: any) => {
    //   //this.messages.push(msg);
    //   console.log(msg.Featlist);
    //   console.log(msg.F1score);

    // });
    // this.socketio.on('relief', (msg: any) => {
    //   //this.messages.push(msg);
    //   console.log(msg.Featlist);
    //   console.log(msg.F1score);

    // });




    this.categoriesSecondary = []

    this.algoPerformance = []

    this.algorithmList = [
      {
        algoName:"CPPS WP 3.1",
        selected: false
      },
      {
        algoName:"Relief",
        selected: false
      },
      {
        algoName:"FCBF",
        selected: false
      },
      {
        algoName:"JMIM",
        selected: false
      }

    ]

    this.datasetList = [
      {
        datasetName: "Academic 1",  //"Colon",
        selected: false
      },
      {
        datasetName: "Academic 2",  //"Dermatology",
        selected: false
      },
      // {
      //   datasetName: "AFPD",  //was Yale
      //   selected: false,
      // },
      {
        datasetName: "Industrial", //"Agilent",
        selected: false
      },
    ]

    this.f1AvgScore = [
      {
        algorithm: this.algorithmList[0].algoName,  //Hybrid
        dataset: this.datasetList[0].datasetName,      //Colon
        accuracy: 90,
        f1Avg: 90 //85.27  //76.87
      },
      {
        algorithm: this.algorithmList[0].algoName,  //Hybrid
        dataset: this.datasetList[1].datasetName,      //Derma
        accuracy: 98,
        f1Avg: 98 //83.32  //65
      },
      // {
      //   algorithm: this.algorithmList[0].algoName,  //Hybrid
      //   dataset: this.datasetList[2].datasetName,      //AFPD LCD
      //   accuracy: 89, //0,
      //   f1Avg: 89 //84.95, //69.44
      // },
      {
        algorithm: this.algorithmList[0].algoName,  //Hybrid
        dataset: this.datasetList[2].datasetName,      //Agilent
        accuracy: 87,
        f1Avg: 87 //96.96  //81.95,
      },
      {
        algorithm: this.algorithmList[1].algoName,  //Relief
        dataset: this.datasetList[0].datasetName,      //Colon
        accuracy: 78.38,
        f1Avg: 83 //79.64  //78.38
      },
      {
        algorithm: this.algorithmList[1].algoName,  //Relief
        dataset: this.datasetList[1].datasetName,      //Derma
        accuracy: 65,
        f1Avg: 97 //83.12  //65
      },
      // {
      //   algorithm: this.algorithmList[1].algoName,  //Relief
      //   dataset: this.datasetList[2].datasetName,      //AFPD
      //   accuracy: 72.33, //1.25,
      //   f1Avg: 85 //84.24  //70.51
      // },
      {
        algorithm: this.algorithmList[1].algoName,  //Relief
        dataset: this.datasetList[2].datasetName,      //Agilent
        accuracy: 91.49,
        f1Avg: 85 //96.71
      },
      {
        algorithm: this.algorithmList[2].algoName,  //FCBF
        dataset: this.datasetList[0].datasetName,      //Colon
        accuracy: 77.22,
        f1Avg: 83 //85.37
      },
      {
        algorithm: this.algorithmList[2].algoName,  //FCBF
        dataset: this.datasetList[1].datasetName,      //Derma
        accuracy: 80.48,
        f1Avg: 97 //85.23
      },
      // {
      //   algorithm: this.algorithmList[2].algoName,  //FCBF
      //   dataset: this.datasetList[2].datasetName,      //AFPD
      //   accuracy: 75.57, //0,
      //   f1Avg: 84 //82.43
      // },
      {
        algorithm: this.algorithmList[2].algoName,  //FCBF
        dataset: this.datasetList[2].datasetName,      //Agilent
        accuracy: 77.25,
        f1Avg: 80 //82.74
      },
      {
        algorithm: this.algorithmList[3].algoName,  //JMIM
        dataset: this.datasetList[0].datasetName,      //Colon
        accuracy: 77.70,
        f1Avg: 85 //86.56
      },
      {
        algorithm: this.algorithmList[3].algoName,  //JMIM
        dataset: this.datasetList[1].datasetName,      //Derma
        accuracy: 65,
        f1Avg: 83 //89.98
      },
      // {
      //   algorithm: this.algorithmList[3].algoName,  //JMIM
      //   dataset: this.datasetList[2].datasetName,      //AFPD
      //   accuracy: 73.66, //1.25,
      //   f1Avg: 82 //89.26
      // },
      {
        algorithm: this.algorithmList[3].algoName,  //JMIM
        dataset: this.datasetList[2].datasetName,      //Agilent
        accuracy: 94.53,
        f1Avg: 85 //97.53
      }
    ]

    this.selectedFeatures = [
      {
        algorithm: this.algorithmList[0].algoName,  //Relief
        dataset: this.datasetList[0].datasetName,      //Agilent
        features: `59 79 44 19 14 39 24 209 ... 108 241 136 192 207 137`,
       accuracy: this.f1AvgScore[0].accuracy,
       f1Avg: this.f1AvgScore[0].f1Avg
      },
      {
        algorithm: this.algorithmList[0].algoName,  //Relief
        dataset: this.datasetList[1].datasetName,      //Colon
        features: "1733 1755 1751 ...  976  314 1867",
        accuracy: this.f1AvgScore[1].accuracy,
        f1Avg: this.f1AvgScore[1].f1Avg
      },
      // {
      //   algorithm: this.algorithmList[0].algoName,  //Relief
      //   dataset: this.datasetList[2].datasetName,      //AFPD
      //   features: "32 864 896 ... 112  81 152",
      //   accuracy: this.f1AvgScore[2].accuracy,
      //   f1Avg: this.f1AvgScore[2].f1Avg
      // },
      {
        algorithm: this.algorithmList[0].algoName,  //Relief
        dataset: this.datasetList[2].datasetName,      //Dermatology
        features: `0 6 25 31 13 30 27 18 15 3 4 1 14 29 28 12 7 5 19 22 20 9 32 17
        26 24 2 16 33 8 10 11 21 23`,
        accuracy: this.f1AvgScore[2].accuracy,
        f1Avg: this.f1AvgScore[2].f1Avg
      },
      {
        algorithm: this.algorithmList[1].algoName,  //FCBF
        dataset: this.datasetList[0].datasetName,      //Agilent
        features: "51 233 231 205",
        accuracy: this.f1AvgScore[3].accuracy,
        f1Avg: this.f1AvgScore[3].f1Avg
      },
      {
        algorithm: this.algorithmList[1].algoName,  //FCBF
        dataset: this.datasetList[1].datasetName,      //Colon
        features: "1358 0",
        accuracy: this.f1AvgScore[4].accuracy,
        f1Avg: this.f1AvgScore[4].f1Avg
      },
      {
        algorithm: this.algorithmList[1].algoName,  //FCBF
        dataset: this.datasetList[2].datasetName,      //AFPD
        features: "522 0",
        accuracy: this.f1AvgScore[5].accuracy,
        f1Avg: this.f1AvgScore[5].f1Avg
      },
      // {
      //   algorithm: this.algorithmList[1].algoName,  //FCBF
      //   dataset: this.datasetList[3].datasetName,      //Dermatology
      //   features: "20 21 32 14  8 27 15 13 30 25 4 33  2  1  3  0 17 16 12",
      //   accuracy: this.f1AvgScore[7].accuracy,
      //   f1Avg: this.f1AvgScore[7].f1Avg
      // },
      {
        algorithm: this.algorithmList[2].algoName,  //JMIM
        dataset: this.datasetList[0].datasetName,      //Agilent
        features: "65 216 214 232 194 178 222 149 212 238",
        accuracy: this.f1AvgScore[6].accuracy,
        f1Avg: this.f1AvgScore[6].f1Avg
      },
      {
        algorithm: this.algorithmList[2].algoName,  //JMIM
        dataset: this.datasetList[1].datasetName,      //Colon
        features: "625 1873 1211 1772 1582 765 249 581 493 1671",
        accuracy: this.f1AvgScore[7].accuracy,
        f1Avg: this.f1AvgScore[7].f1Avg
      },
      {
        algorithm: this.algorithmList[2].algoName,  //JMIM
        dataset: this.datasetList[2].datasetName,      //Agilent
        features: "224 195 800 896 512 794 403 671 161 32",
        accuracy: this.f1AvgScore[8].accuracy,
        f1Avg: this.f1AvgScore[8].f1Avg
      },
      // {
      //   algorithm: this.algorithmList[2].algoName,  //JMIM
      //   dataset: this.datasetList[3].datasetName,      //Dermatology
      //   features: "33 20 15 19 21 32 27 5 26 28",
      //   accuracy: this.f1AvgScore[11].accuracy,
      //   f1Avg: this.f1AvgScore[11].f1Avg
      // },
      {
        algorithm: this.algorithmList[3].algoName,  //Hybrid
        dataset: this.datasetList[0].datasetName,      //Agilent
        features: `51 233 59 19 79 39 14 44 24 125 64 4 209 216 132 58 32 34
        38 66 167 6`,
        accuracy: this.f1AvgScore[9].accuracy,
        f1Avg: this.f1AvgScore[9].f1Avg
      },
      {
        algorithm: this.algorithmList[3].algoName,  //Hybrid
        dataset: this.datasetList[1].datasetName,      //Colon
        features: "1358 0 1733 ... 1212 258 1823",
        accuracy: this.f1AvgScore[10].accuracy,
        f1Avg: this.f1AvgScore[10].f1Avg
      },
      {
        algorithm: this.algorithmList[3].algoName,  //Hybrid
        dataset: this.datasetList[2].datasetName,      //Agilent
        features: "522 0",
        accuracy: this.f1AvgScore[11].accuracy,
        f1Avg: this.f1AvgScore[11].f1Avg
      },
      // {
      //   algorithm: this.algorithmList[3].algoName,  //Hybrid
      //   dataset: this.datasetList[3].datasetName,      //Dermatology
      //   features: "20 21 32 14 8 27 15 13 0 6",
      //   accuracy: this.f1AvgScore[15].accuracy,
      //   f1Avg: this.f1AvgScore[15].f1Avg
      // }

    ];

    this.toggle('CPPS WP 3.1','algorithm');
    this.toggle('Relief','algorithm');
    this.toggle('FCBF','algorithm');
    this.toggle('JMIM','algorithm');

    this.toggle('Academic 1', 'dataset');  //academic 1: colon
    this.toggle('Academic 2', 'dataset');  //Dermatology
    //this.toggle('AFPD', 'dataset');
    this.toggle('Industrial', 'dataset');  //'Agilent'

    this.cbAlgoHybrid = true;
    this.cbAlgoRelief = true
    this.cbAlgoFCBF = true;
    this.cbAlgoJMIM = true;
    this.cbDatasetColon = true;
    this.cbDatasetDermatology = true;
    //this.cbDatasetAFPD = true;
    this.cbDatasetAgilent = true;

  }


  // socketConnect(){
  //   this.socketio = io.connect('http://127.0.0.1:105');
  //   //this.socketio2 = io.connect('http://127.0.0.1:105');
  //   }



    // monitorRelief(){
      
    //   this.socketio.on('reliefResult', (msg: any) => {
    //     console.log(msg);
    //     //console.log(msg.Featlist);
    //     //console.log(msg.F1score);
    //   });
    // }
    

    // monitorFCBF(){   


    //   this.socketio.on('fcbfResult', (msg: any) => {
    //     //console.log(msg);
    //     console.log(msg.Featlist);
    //     console.log(msg.F1score)
    //   });

    // }



    // monitorJMIM(){
    
    //   this.socketio.on('result', (msg: any) => {
    //     //console.log(msg);
    //     console.log(msg.Featlist);
    //     console.log(msg.F1score)
    //   });
    // }




  public showSelectedFeaturesTable() {
      this.showSelectedFeatures = true;
  }

  public closeSelectedFeaturesWindow(){
    this.showSelectedFeatures = false;
    window.scrollTo(0,0);
  }



  public toggle(itemName: string, selectionType: string){
    if (selectionType == "algorithm"){
      this.UpdateChartAlgorithmChange(itemName);
    }
    else if (selectionType == "dataset"){
      this.UpdateChartDatasetChange(itemName);
    }
  }


  private UpdateChart(chartData: any){
    this.algoCategories = [...this.categoriesSecondary];   //was algoCategories
    this.chartData = [...chartData];
    //this.datasetCategories = [...this.categoriesSecondary]
  }


  private UpdateChartAlgorithmChange(algorithm: string){

    let algorithmSelected = this.algorithmList.filter(x=>x.algoName == algorithm);

    if (algorithmSelected[0].selected == false){

      for(let i =0; i<this.algorithmList.length;i++){
        if (this.algorithmList[i].algoName == algorithm){
          this.algorithmList[i].selected = true
        }
      }

      //algorithmSelected[0].selected = true;
      //this.categoriesPrimary.push(algorithm);

      let categoriesSecondary = [];
      if (this.datasetList.filter(x => x.selected == true).length > 0){

        this.datasetList.forEach(function(item,index){
          if (item.selected == true){
            categoriesSecondary.push(item.datasetName);
          }
        })
        //check which dataset is already selected
        //then add the f1 score for this algorithm and datasets to chart
        //let categoriesSecondary = Array.from(this.categoriesSecondary);
        let f1AvgScore = Array.from(this.f1AvgScore);
        let chartData = [];

        let algorithmArray = [{
            algoName:"",
            selected: false
          },
          {
            algoName:"",
            selected:false
          },
          {
            algoName:"",
            selected:false
          },
          {
            algoName:"",
            selected:false
          }
        ];

        this.algorithmList.forEach(function(item, index){
          if (item.algoName == "CPPS WP 3.1"){
            algorithmArray[0].algoName = item.algoName;
            algorithmArray[0].selected = item.selected;
          }
          else if (item.algoName == "Relief"){
            algorithmArray[1].algoName = item.algoName;
            algorithmArray[1].selected = item.selected;
          }
          else if (item.algoName == "FCBF"){
            algorithmArray[2].algoName = item.algoName;
            algorithmArray[2].selected = item.selected;
          }
          else if (item.algoName == "JMIM"){
            algorithmArray[3].algoName = item.algoName;
            algorithmArray[3].selected = item.selected;
          }
        })

        algorithmArray.forEach(function(item, index){
          if (item.selected == true){
            let dataArray = [];

            for(let i=0; i<categoriesSecondary.length; i++){
              let f1 = f1AvgScore.filter(x => x.algorithm == item.algoName && x.dataset == categoriesSecondary[i])
              if (f1.length > 0){
                let f1Avg = f1[0].f1Avg;
                dataArray.push(f1Avg);
              }
            }
            chartData.push({data: dataArray, name: item.algoName})
          }
        })
        this.UpdateChart(chartData);
      }
    }
    else if (algorithmSelected[0].selected == true){
      for(let i =0; i<this.algorithmList.length;i++){
        if (this.algorithmList[i].algoName == algorithm){
          this.algorithmList[i].selected = false;
        }
      }

      //let index = this.categoriesPrimary.indexOf(algorithm,0);
      //this.categoriesPrimary.splice(index,1);

      //update Chart
      let f1AvgScore = Array.from(this.f1AvgScore);
      let chartData = [];
      let categoriesSecondary = Array.from(this.categoriesSecondary);

      let algorithmArray = [{
        algoName:"",
        selected: false
      },
      {
        algoName:"",
        selected:false
      },
      {
        algoName:"",
        selected:false
      },
      {
        algoName:"",
        selected:false
      }
      ];

      this.algorithmList.forEach(function(item, index){
        if (item.algoName == "CPPS WP 3.1"){
          algorithmArray[0].algoName = item.algoName;
          algorithmArray[0].selected = item.selected;
        }
        else if (item.algoName == "Relief"){
          algorithmArray[1].algoName = item.algoName;
          algorithmArray[1].selected = item.selected;
        }
        else if (item.algoName == "FCBF"){
          algorithmArray[2].algoName = item.algoName;
          algorithmArray[2].selected = item.selected;
        }
        else if (item.algoName == "JMIM"){
          algorithmArray[3].algoName = item.algoName;
          algorithmArray[3].selected = item.selected;
        }
      })

      algorithmArray.forEach(function(item, index){
        if (item.selected == true){
          let dataArray = [];

          for(let i=0; i<categoriesSecondary.length; i++){
            let f1 = f1AvgScore.filter(x => x.algorithm == item.algoName && x.dataset == categoriesSecondary[i])
            if (f1.length > 0){
              let f1Avg = f1[0].f1Avg;
              dataArray.push(f1Avg);
            }
          }
          chartData.push({data: dataArray, name: item.algoName})
        }
      })
      this.UpdateChart(chartData);
    }

  }


  public dscategories:Array<string> = []


  private UpdateChartDatasetChange(dataset: string){

    let self = this;
    let datasetSelected = this.datasetList.filter(x=>x.datasetName == dataset);
    //let categories = [];

    if (datasetSelected[0].selected == false){
      for(let i =0; i<this.datasetList.length;i++){
        if (this.datasetList[i].datasetName == dataset){
          this.datasetList[i].selected = true;
          if (!self.dscategories.includes(dataset)) self.dscategories.push(dataset);
        }
        // else {
        //   if (!self.dscategories.includes(this.datasetList[i].datasetName)) self.dscategories.push(this.datasetList[i].datasetName);
        // }
      }

      let categoriesReorder = []
      let datasetRef = ['Academic 1', 
            'Academic 2',
            'Industrial'];  //"Colon", "Dermatology", "Agilent"
      datasetRef.forEach(function(dataitem, index){
        self.dscategories.forEach(function(item, index){
          if (item == dataitem){
            categoriesReorder.push(item);
          }
        })
      })


      if (this.algorithmList.filter(x => x.selected == true).length > 0){
        //let algoNames = this.algorithmList.filter(x => x.selected == true).map(x => x.algoName)
        this.categoriesSecondary = Array.from(categoriesReorder);

        //check which dataset is already selected
        //then add the f1 score for this algorithm and datasets to chart
        //let categoriesPrimary = Array.from(this.categoriesPrimary);
        let f1AvgScore = Array.from(this.f1AvgScore);
        let chartData = [];

        this.algorithmList.forEach(function(item, index){
          if (item.selected == true){
            let dataArray = [];

            for(let i=0; i<categoriesReorder.length; i++){
              let f1 = f1AvgScore.filter(x => x.algorithm == item.algoName && x.dataset == categoriesReorder[i])
              if (f1.length > 0){
                let f1Avg = f1[0].f1Avg;
                dataArray.push(f1Avg);
              }
              //let f1Avg  = f1AvgScore.filter(x => x.dataset == item.datasetName && x.algorithm == categoriesPrimary[i])[0].f1Avg;

            }
            chartData.push({data: dataArray, name: item.algoName})

          }
        })
        this.UpdateChart(chartData);
      }

    }
    else {
      datasetSelected[0].selected = false;

      self.dscategories = self.dscategories.filter(x => x != dataset);

      for(let i =0; i<this.datasetList.length;i++){
        if (this.datasetList[i].datasetName == dataset){
          this.datasetList[i].selected = false;
          let index = this.categoriesSecondary.indexOf(dataset,0);
          this.categoriesSecondary.splice(index,1);
        }
      }


      let categories = [...this.categoriesSecondary];
      let categoriesReorder = [];
      let datasetRef = ['Academic 1', 'Academic 2', 'Industrial'];   //"Colon", "Dermatology", "Agilent"
      datasetRef.forEach(function(dataitem, index){
        categories.forEach(function(item, index){
          if (item == dataitem){
            categoriesReorder.push(item)
          }
        })
      })

      if (this.algorithmList.filter(x => x.selected == true).length > 0){
        //let algoNames = this.algorithmList.filter(x => x.selected == true).map(x => x.algoName)
        this.categoriesSecondary = [];
        this.categoriesSecondary = Array.from(categoriesReorder);

        //check which dataset is already selected
        //then add the f1 score for this algorithm and datasets to chart
        //let categoriesPrimary = Array.from(this.categoriesPrimary);
        let f1AvgScore = Array.from(this.f1AvgScore);
        let chartData = [];

        this.algorithmList.forEach(function(item, index){
          if (item.selected == true){
            let dataArray = [];

            for(let i=0; i<categoriesReorder.length; i++){
              let f1 = f1AvgScore.filter(x => x.algorithm == item.algoName && x.dataset == categoriesReorder[i])
              if (f1.length > 0){
                let f1Avg = f1[0].f1Avg;
                dataArray.push(f1Avg);
              }
              //let f1Avg  = f1AvgScore.filter(x => x.dataset == item.datasetName && x.algorithm == categoriesPrimary[i])[0].f1Avg;

            }
            chartData.push({data: dataArray, name: item.algoName})

          }
        })
        this.UpdateChart(chartData);
      }
    }


    ///////////////////////////////////////////////////////////////////






  }

  //call api
  public runFCBF(){

    this.socketio.emit("fcbf", "test");

    // const headers = new HttpHeaders()
    // .set('content-type', 'application/json')
    // .set('Access-Control-Allow-Origin', 'http://localhost:4200')
    // this.http.get("http://127.0.0.1:105/fcbf", {'headers': headers}).subscribe((msg:any) => {
    // }
    // )
  }

  public runAFPD(){

    const headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', 'http://localhost:4200')

    // forkJoin({
    //   hybrid: this.http.get("http://127.0.0.1:105/hybrid", {'headers': headers}),
    //   relief: this.http.get("http://127.0.0.1:105/relief", {'headers': headers}),
    //   jmim: this.http.get("http://127.0.0.1:105/jmim", {'headers': headers}),
    //   fcbf: this.http.get("http://127.0.0.1:105/fcbf", {'headers': headers})
    //   //.pipe(map(res => console.log(res)),
    // }).subscribe(allResults => console.log(allResults));

    this.http.get("http://localhost:105/featselection/hybrid", {'headers': headers}).subscribe((msg:any) => {
      this.algoPerformance.push({ Algorithm: msg.Algorithm, Featlist: msg.Featlist, F1score: msg.F1score});
      console.log(this.algoPerformance);
      this.showSelectedFeatures = true;
    })

    this.http.get("http://localhost:105/featselection/relief", {'headers': headers}).subscribe((msg:any) => {

      this.algoPerformance.push({ Algorithm: msg.Algorithm, Featlist: msg.Featlist, F1score: msg.F1score});
      console.log(this.algoPerformance);
    })

    this.http.get("http://localhost:105/featselection/jmim", {'headers': headers}).subscribe((msg:any) => {
      this.algoPerformance.push({ Algorithm: msg.Algorithm, Featlist: msg.Featlist, F1score: msg.F1score});
      console.log(this.algoPerformance);
    })

    this.http.get("http://localhost:105/featselection/fcbf", {'headers': headers}).subscribe((msg:any) => {
      this.algoPerformance.push({ Algorithm: msg.Algorithm, Featlist: msg.Featlist, F1score: msg.F1score});
      console.log(this.algoPerformance);
    })
  }


  public showAFPDResults(){
    this.showSelectedFeatures = true;
  }
}
