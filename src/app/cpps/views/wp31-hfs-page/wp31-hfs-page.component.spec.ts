import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wp31HfsPageComponent } from './wp31-hfs-page.component';

describe('Wp31HfsPageComponent', () => {
  let component: Wp31HfsPageComponent;
  let fixture: ComponentFixture<Wp31HfsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Wp31HfsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wp31HfsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
