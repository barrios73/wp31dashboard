import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wp31ResultsPageComponent } from './wp31-results-page.component';

describe('Wp31ResultsPageComponent', () => {
  let component: Wp31ResultsPageComponent;
  let fixture: ComponentFixture<Wp31ResultsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Wp31ResultsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wp31ResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
