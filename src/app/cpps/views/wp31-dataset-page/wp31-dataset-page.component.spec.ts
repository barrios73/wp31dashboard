import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wp31DatasetPageComponent } from './wp31-dataset-page.component';

describe('Wp31DatasetPageComponent', () => {
  let component: Wp31DatasetPageComponent;
  let fixture: ComponentFixture<Wp31DatasetPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Wp31DatasetPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wp31DatasetPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
