import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wp31-dataset-page',
  templateUrl: './wp31-dataset-page.component.html',
  styleUrls: ['./wp31-dataset-page.component.scss']
})
export class Wp31DatasetPageComponent implements OnInit {

  public pageTitle: string

  constructor() { 
    this.pageTitle = "Dataset"
  }

  ngOnInit(): void {
  }

}
