import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wp31-introduction-page',
  templateUrl: './wp31-introduction-page.component.html',
  styleUrls: ['./wp31-introduction-page.component.scss']
})
export class Wp31IntroductionPageComponent implements OnInit {

  public pageTitle: string;

  constructor() { 
    this.pageTitle = "Introduction"
  }

  ngOnInit(): void {
  }


}
