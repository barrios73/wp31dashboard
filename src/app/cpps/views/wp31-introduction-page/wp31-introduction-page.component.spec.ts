import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Wp31IntroductionPageComponent } from './wp31-introduction-page.component';

describe('Wp31IntroductionPageComponent', () => {
  let component: Wp31IntroductionPageComponent;
  let fixture: ComponentFixture<Wp31IntroductionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Wp31IntroductionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Wp31IntroductionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
