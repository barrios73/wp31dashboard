import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WpsOutcomesPageComponent } from './wps-outcomes-page.component';

describe('WpsOutcomesPageComponent', () => {
  let component: WpsOutcomesPageComponent;
  let fixture: ComponentFixture<WpsOutcomesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WpsOutcomesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WpsOutcomesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
