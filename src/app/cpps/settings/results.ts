
export const algorithms = [
    {
        algoName:"Relief",     
        selected: false
      },
      {
        algoName:"FCBF",
        selected: false
      },
      {
        algoName:"JMIM",
        selected: false
      },
      {
        algoName:"Hybrid (Proposed)",
        selected: false
      }
]


export const datasets = [
    {        
        datasetName: "Agilent",
        selected: false        
      },
      {        
        datasetName: "Colon",
        selected: false
      },
      {
        datasetName: "Yale",
        selected: false,
      },
      {        
        datasetName: "Dermatology",
        selected: false
      }

]


export const f1AvgScoreByAlgoDataset = [
    {
        algorithm: algorithms[0].algoName,  //Relief
        dataset: datasets[0].datasetName,      //Agilent
        f1Avg: 90.35
      },
      {
        algorithm: algorithms[0].algoName,  //Relief
        dataset: datasets[1].datasetName,      //Colon
        f1Avg: 89.02
      },
      {
        algorithm: algorithms[0].algoName,  //Relief
        dataset: datasets[2].datasetName,      //Yale
        f1Avg: 88.66
      },
      {
        algorithm: algorithms[0].algoName,  //Relief
        dataset: datasets[3].datasetName,      //Dermatology
        f1Avg: 91.16
      },
      {
        algorithm: algorithms[1].algoName,  //FCBF
        dataset: datasets[0].datasetName,      //Agilent
        f1Avg: 91.12
      },
      {
        algorithm: algorithms[1].algoName,  //FCBF
        dataset: datasets[1].datasetName,      //Colon
        f1Avg: 89.88
      },
      {
        algorithm: algorithms[1].algoName,  //FCBF
        dataset: datasets[2].datasetName,      //Yale
        f1Avg: 90.24
      },
      {
        algorithm: algorithms[1].algoName,  //FCBF
        dataset: datasets[3].datasetName,      //Dermatology
        f1Avg: 90.24
      },
      {
        algorithm: algorithms[2].algoName,  //JMIM
        dataset: datasets[0].datasetName,      //Agilent
        f1Avg: 89.33
      },
      {
        algorithm: algorithms[2].algoName,  //JMIM
        dataset: datasets[1].datasetName,      //Colon
        f1Avg: 91.41
      },
      {
        algorithm: algorithms[2].algoName,  //JMIM
        dataset: datasets[2].datasetName,      //Yale
        f1Avg: 90.23
      },
      {
        algorithm: algorithms[2].algoName,  //JMIM
        dataset: datasets[3].datasetName,      //Dermatology
        f1Avg: 92.54
      },
      {
        algorithm: algorithms[3].algoName,  //Hybrid
        dataset: datasets[0].datasetName,      //Agilent
        f1Avg: 92.19
      },
      {
        algorithm: algorithms[3].algoName,  //Hybrid
        dataset: datasets[1].datasetName,      //Colon
        f1Avg: 91.69
      },
      {
        algorithm: algorithms[3].algoName,  //Hybrid
        dataset: datasets[2].datasetName,      //Yale
        f1Avg: 93.04
      },
      {
        algorithm: algorithms[3].algoName,  //Hybrid
        dataset: datasets[3].datasetName,      //Dermatology
        f1Avg: 92.44
      }
    ]