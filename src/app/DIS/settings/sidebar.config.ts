import { RoleTypes } from '@dis/auth/roles.enum';
// Will allow show/hide of links in sidebar when sign-on flow is implemented

export const config = [
  // Add navigation group here
  // {
  //   group: 'Research (samples)',
  //   // Add navigation items here
  //   items: [
  //     {
  //       name: 'Overview (sample)',
  //       icon: 'information',
  //       link: './overview-new',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Dataset (sample)',
  //       icon: 'information',
  //       link: './dataset-new',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Outcomes (sample)',
  //       icon: 'information',
  //       link: './outcome-new',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     // {
  //     //   name: 'Introduction',
  //     //   icon: 'information',
  //     //   link: './introduction',
  //     //   elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     // },
  //     // {
  //     //   name: 'Results',
  //     //   icon: 'graph',
  //     //   link: './results',
  //     //   elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     // },
  //     // {
  //     //   name: 'Conclusion',
  //     //   icon: 'crosstab',
  //     //   link: './conclusion',
  //     //   elevation: []
  //     // }
  //   ]
  // },
  // {
  //   group: 'Research (old)',
  //   // Add navigation items here
  //   items: [
  //     {
  //       name: 'Overview',
  //       icon: 'graph',
  //       link: './wp31-introduction-page',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Dataset',
  //       icon: 'graph',
  //       link: './wp31-dataset-page',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Solution',
  //       icon: 'graph',
  //       link: './wp31-hfs-page',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Results',
  //       icon: 'graph',
  //       link: './wp31-results-page',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     },
  //     {
  //       name: 'Outcomes',
  //       icon: 'graph',
  //       link: './wp31-conclusion-page',
  //       elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //     }
  //   ]
  // },
  {
    group: 'Research',
    // Add navigation items here
    items: [
      {
        name: 'Overview ',
        icon: 'info-circle',
        link: './wps-overview-page',
        elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
      },
      {
        name: 'Outcomes ',
        icon: 'subreport',
        link: './wps-outcomes-page',
        elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
      },
      {
        name: 'Results ',
        icon: 'graph',
        link: './wps-results-page',
        elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
      },
      /* {
        // to be placed under results
        name: 'Dataset (working)',
        icon: 'validation-data',
        link: './wps-dataset-page',
        elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
      }, */
      {
        name: 'Conclusion ',
        icon: 'file-ascx',
        link: './wps-conclusion-page',
        elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
      },
    ]
  },
  // {
  // group: 'Sample Pages',
  // Add navigation items here
  // items: [
  // {
  //   name: 'Introduction',
  //   icon: 'information',
  //   link: './introduction-no-dataset',
  //   elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  // },
  // {
  //   name: 'Dataset',
  //   icon: 'graph',
  //   link: './dataset',
  //   elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  // },
  // {
  //   name: 'Results',
  //   icon: 'graph',
  //   link: './table',
  //   elevation: [] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  // },
  // {
  //   name: 'Conclusion',
  //   icon: 'crosstab',
  //   link: './conclusion',
  //   elevation: []
  // }
  // ]
  // },
];
