import { addonEnvironment } from '@dis/settings/environments/environment.prod';

const KEYCLOAK_URL = 'http:192.168.137.1:8080'; //'http://localhost:8080';

export const environment = {
  production: false,

  DEV_TEST_USER: {
    id: 'Dev User 1',
    username: 'devuser1',
    email: 'devuser1@test.com',
    firstName: 'dev',
    lastName: 'user',
    enabled: true,
    emailVerified: true,
    totp: true
  },

  KEYCLOAK_URL: KEYCLOAK_URL + '/auth',
  KEYCLOAK_REALM: 'demo1',
  KEYCLOAK_CLIENT: 'tr-pqm-client',
  API_ROOT: 'http://localhost:5000',  //don't change
  APP_ROOT: 'http://192.168.137.1/InsituQuality', //'http://localhost:4200',   //'http://localhost:4200',  //frontend URL 
  KEYCLOAK_GET_CLIENT_ROLES_1: KEYCLOAK_URL + '/auth/admin/realms/demo1/users/',
  KEYCLOAK_GET_CLIENT_ROLES_2: '/role-mappings/clients/',
  KEYCLOAK_GET_CLIENT_ROLES_3: '/composite',
  ...addonEnvironment
};

